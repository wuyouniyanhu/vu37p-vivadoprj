#set_property IOSTANDARD DIFF_SSTL12 [get_ports clk_in1_p]
#set_property BOARD_PART_PIN default_100mhz_clk_n [get_ports clk_in1_n]
#set_property BOARD_PART_PIN default_100mhz_clk_p [get_ports clk_in1_p]
set_property PACKAGE_PIN BH51 [get_ports clk_in1_p]
set_property PACKAGE_PIN BJ51 [get_ports clk_in1_n]
set_property IOSTANDARD LVDS [get_ports clk_in1_p]
set_property IOSTANDARD LVDS [get_ports clk_in1_n]
create_clock -period 10.000 [get_ports clk_in1_p]
#set_property IOSTANDARD LVDS [get_ports clk_in1_n]
#set_property PACKAGE_PIN BH51 [get_ports clk_in1_p]
#set_property PACKAGE_PIN BJ51 [get_ports clk_in1_n]

#create_clock -period 10.000 [get_ports RefCLK]
set_property PACKAGE_PIN BJ4 [get_ports DDR_SYS_CLK_p]
#set_property CLOCK_DEDICATED_ROUTE SAME_CMT_COLUMN [get_nets u_m_clk_src/util_ds_buf_0/U0/IBUF_OUT[0]]
set_property CLOCK_DEDICATED_ROUTE SAME_CMT_COLUMN [get_nets -of [get_pins GCIO_CLK_IN/O]]
#set_property PACKAGE_PIN BK3 [get_ports DDR_SYS_CLK_n]
#set_property IOSTANDARD LVDS [get_ports DDR_SYS_CLK_p]
#set_property IOSTANDARD LVDS [get_ports DDR_SYS_CLK_n]
#set_property IOSTANDARD DIFF_SSTL12 [get_ports clk_in1_n]

#set_property LOC MMCM_X0Y0 [get_cells -hier -filter {NAME =~ u_hbm_clk}]
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets {u_clk_src/util_ds_buf_0/U0/BUFG_O[0]}]
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets u_clk_src/util_ds_buf_1/U0/IBUF_OUT]
#-------------- MCS Generation ----------------------
#set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN div-1  [current_design]
#set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES       [current_design]
#set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4          [current_design]
#set_property BITSTREAM.GENERAL.COMPRESS TRUE          [current_design]
#set_property BITSTREAM.CONFIG.UNUSEDPIN Pullnone      [current_design]
#set_property CFGBVS GND                               [current_design]
#set_property CONFIG_VOLTAGE 1.8                       [current_design]
#set_property CONFIG_MODE SPIx4                        [current_design]

set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 63.8 [current_design]
set_property BITSTREAM.CONFIG.SPI_OPCODE 8'h6B [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN Pulldown [current_design]
set_property CONFIG_VOLTAGE 1.8 [current_design]

# Following are the SPI device parameters
# Max Tco
# Min Tco

# Setup time requirement

# Hold time requirement


#set_property PACKAGE_PIN BF22 [get_ports SPI_0_io0_io]
#set_property IOSTANDARD LVCMOS18 [get_ports SPI_0_io0_io]
#
#set_property PACKAGE_PIN BF21 [get_ports SPI_0_io1_io]
#set_property IOSTANDARD LVCMOS18 [get_ports SPI_0_io1_io]
#
#set_property PACKAGE_PIN IOB_X0Y207 [get_ports SPI_0_io2_io]
#set_property IOSTANDARD LVCMOS18 [get_ports SPI_0_io2_io]
#
#set_property PACKAGE_PIN IOB_X0Y208 [get_ports SPI_0_io3_io]
#set_property IOSTANDARD LVCMOS18 [get_ports SPI_0_io3_io]
#
#set_property PACKAGE_PIN BJ21 [get_ports SPI_0_ss_io]
#set_property IOSTANDARD LVCMOS18 [get_ports SPI_0_ss_io]
#
#set_property PACKAGE_PIN BH21 [get_ports SPI_0_sck_io]
#set_property IOSTANDARD LVCMOS18 [get_ports SPI_0_sck_io]

# Bank: 40 - GPIO_LED0
set_property PACKAGE_PIN BH24 [get_ports {led[0]}]

# Bank: 40 - GPIO_LED1
set_property PACKAGE_PIN BG24 [get_ports {led[1]}]

# Bank: 40 - GPIO_LED2
set_property PACKAGE_PIN BG25 [get_ports {led[2]}]

# Bank: 40 - GPIO_LED3
set_property PACKAGE_PIN BF25 [get_ports {led[3]}]

# Bank: 40 - GPIO_LED4
set_property PACKAGE_PIN BF26 [get_ports {led[4]}]

# Bank: 40 - GPIO_LED5
set_property PACKAGE_PIN BF27 [get_ports {led[5]}]

# Bank: 40 - GPIO_LED6
set_property PACKAGE_PIN BG27 [get_ports {led[6]}]

# Bank: 40 - GPIO_LED7
set_property PACKAGE_PIN BG28 [get_ports {led[7]}]



#set_property IOSTANDARD SSTL12_DCI [get_ports {ddr4_sdram_cs_n[0]}]

#connect_debug_port u_ila_0/clk [get_nets [list u_clk_src/clk_wiz_0/inst/clk_out1]]


#set_property LOC MMCM_X0Y2 [get_cells top_wrapper/top_i/ddr4_0/inst/u_ddr4_infrastructure/gen_mmcme4.u_mmcme_adv_inst]

#set_property IOSTANDARD LVCMOS18 [get_ports RefCLK]
#set_property IOSTANDARD LVCMOS18 [get_ports clk_in1_n]

####################################################################################
# Constraints from file : 'bd_76f1_eth_buf_0.xdc'
####################################################################################


#set_property LOC MMCM_X0Y2 [get_cells u_ddr4/inst/u_ddr4_infrastructure/gen_mmcme4.u_mmcme_adv_inst]

create_clock -period 10.000 [get_ports DDR_SYS_CLK_p]
set_property IOSTANDARD LVCMOS18 [get_ports DDR_SYS_CLK_p]
create_generated_clock -name clk_sck -source [get_pins -hierarchical *axi_quad_spi_0/ext_spi_clk] -edges {3 5 7} [get_pins -hierarchical */CCLK]
set_input_delay -clock clk_sck -clock_fall -max 7.450 [get_pins -hierarchical {*STARTUP*/DATA_IN[*]}]
set_input_delay -clock clk_sck -clock_fall -min 1.450 [get_pins -hierarchical {*STARTUP*/DATA_IN[*]}]
set_output_delay -clock clk_sck -max 2.050 [get_pins -hierarchical {*STARTUP*/DATA_OUT[*]}]
set_output_delay -clock clk_sck -min -2.950 [get_pins -hierarchical {*STARTUP*/DATA_OUT[*]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led[0]}]
set_property DRIVE 8 [get_ports {led[0]}]
set_property SLEW SLOW [get_ports {led[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led[1]}]
set_property DRIVE 8 [get_ports {led[1]}]
set_property SLEW SLOW [get_ports {led[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led[2]}]
set_property DRIVE 8 [get_ports {led[2]}]
set_property SLEW SLOW [get_ports {led[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led[3]}]
set_property DRIVE 8 [get_ports {led[3]}]
set_property SLEW SLOW [get_ports {led[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led[4]}]
set_property DRIVE 8 [get_ports {led[4]}]
set_property SLEW SLOW [get_ports {led[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led[5]}]
set_property DRIVE 8 [get_ports {led[5]}]
set_property SLEW SLOW [get_ports {led[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led[6]}]
set_property DRIVE 8 [get_ports {led[6]}]
set_property SLEW SLOW [get_ports {led[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {led[7]}]
set_property DRIVE 8 [get_ports {led[7]}]
set_property SLEW SLOW [get_ports {led[7]}]

####################################################################################
# Constraints from file : 'bd_76f1_eth_buf_0.xdc'
####################################################################################

#current_instance u_ddr4/inst
#set_property LOC MMCM_X0Y1 [get_cells -hier -filter {NAME =~ */u_ddr4_infrastructure/gen_mmcme*.u_mmcme_adv_inst}]
#current_instance -quiet
set_property INTERNAL_VREF 0.84 [get_iobanks 66]
set_property INTERNAL_VREF 0.84 [get_iobanks 65]
set_property INTERNAL_VREF 0.84 [get_iobanks 64]



create_pblock pblock_shift
add_cells_to_pblock [get_pblocks pblock_shift] [get_cells -quiet [list shift]]
resize_pblock [get_pblocks pblock_shift] -add {SLICE_X122Y160:SLICE_X145Y179}
resize_pblock [get_pblocks pblock_shift] -add {RAMB18_X9Y64:RAMB18_X9Y71}
resize_pblock [get_pblocks pblock_shift] -add {RAMB36_X9Y32:RAMB36_X9Y35}
set_property SNAPPING_MODE ON [get_pblocks pblock_shift]
create_pblock pblock_count
add_cells_to_pblock [get_pblocks pblock_count] [get_cells -quiet [list count]]
resize_pblock [get_pblocks pblock_count] -add {SLICE_X147Y161:SLICE_X159Y177}
resize_pblock [get_pblocks pblock_count] -add {RAMB18_X10Y66:RAMB18_X10Y69}
resize_pblock [get_pblocks pblock_count] -add {RAMB36_X10Y33:RAMB36_X10Y34}
set_property SNAPPING_MODE ON [get_pblocks pblock_count]



