`timescale 1 ps / 1 ps
`define  NUM_DEVICES   1
`define ADDR_WIDTH    21
`define DATA_WIDTH    36
`define _WITH_DDR_
module vcu128_top
(
    /*inout   SPI_0_io0_io,
    inout   SPI_0_io1_io,
    inout   SPI_0_io2_io,
    inout   SPI_0_io3_io,
    inout   SPI_0_sck_io,
    inout   SPI_0_ss_io,*/
`ifdef _WITH_DDR_
    output  ddr4_sdram_act_n,
    output  [16:0] ddr4_sdram_adr,
    output  [1:0] ddr4_sdram_ba,
    output  ddr4_sdram_bg,
    output  ddr4_sdram_ck_c,
    output  ddr4_sdram_ck_t,
    output  ddr4_sdram_cke,
    output  [1:0] ddr4_sdram_cs_n,
    inout   [8:0] ddr4_sdram_dm_n,
    inout   [71:0] ddr4_sdram_dq,
    inout   [8:0] ddr4_sdram_dqs_c,
    inout   [8:0] ddr4_sdram_dqs_t,
    output  ddr4_sdram_odt,
    output  ddr4_sdram_reset_n,
`endif
    //input   default_100mhz_clk_clk_n,
    //input   default_100mhz_clk_clk_p,
    input   reset,
    input   rs232_uart_0_rxd,
    output  rs232_uart_0_txd,
    output [7:0]            led,
    input   dummy_port_in,

    //axi ethernet subsystem
    //output         phy_rst_n,
    input          sgmii_lvds_rxn,
    input          sgmii_lvds_rxp,
    output         sgmii_lvds_txn,
    output         sgmii_lvds_txp,
    input          sgmii_phyclk_clk_n,
    input          sgmii_phyclk_clk_p,
    inout          mdio,
    output         mdc,

    //HBM CLOCK <-- DDR CLOCK
`ifdef _WITH_DDR_
    input           clk_in1_p,
    input           clk_in1_n
`else
    input           RefCLK
`endif
    ,input           DDR_SYS_CLK_p
    //input           DDR_SYS_CLK_n     
);

wire    cpu_clk;
wire    cpu_reset;
// wire    hbm_clk;
// wire    hbm_apb_clk;
wire    ddr4_init_copmlete;
wire    flash_int;
wire    uart_int;
wire    mac_int;
wire    spi_clk;
// wire    hbm_clk1;
wire    RefCLK;
wire    ddr4_RefCLK;
wire memory_clk;
wire s2mm_introut;
wire mm2s_introut;
wire    apb_resetn;

wire        mem_axi_0_aw_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_aw_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [3:0]  mem_axi_0_aw_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [35:0] mem_axi_0_aw_bits_addr; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [7:0]  mem_axi_0_aw_bits_len; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [2:0]  mem_axi_0_aw_bits_size; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [1:0]  mem_axi_0_aw_bits_burst; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_aw_bits_lock; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [3:0]  mem_axi_0_aw_bits_cache; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [2:0]  mem_axi_0_aw_bits_prot; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [3:0]  mem_axi_0_aw_bits_qos; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_w_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_w_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [63:0] mem_axi_0_w_bits_data; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [7:0]  mem_axi_0_w_bits_strb; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_w_bits_last; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_b_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_b_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [3:0]  mem_axi_0_b_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [1:0]  mem_axi_0_b_bits_resp; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_ar_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_ar_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [3:0]  mem_axi_0_ar_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [35:0] mem_axi_0_ar_bits_addr; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [7:0]  mem_axi_0_ar_bits_len; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [2:0]  mem_axi_0_ar_bits_size; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [1:0]  mem_axi_0_ar_bits_burst; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_ar_bits_lock; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [3:0]  mem_axi_0_ar_bits_cache; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [2:0]  mem_axi_0_ar_bits_prot; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [3:0]  mem_axi_0_ar_bits_qos; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_r_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_r_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [3:0]  mem_axi_0_r_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [63:0] mem_axi_0_r_bits_data; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire [1:0]  mem_axi_0_r_bits_resp; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire        mem_axi_0_r_bits_last; // @[:freedom.serve.SERVEiFPGAConfig.fir@503052.4]
wire           mem_axi_1_aw_ready;
wire           mem_axi_1_aw_valid;
wire    [3:0]  mem_axi_1_aw_bits_id;
wire    [35:0] mem_axi_1_aw_bits_addr;
wire    [7:0]  mem_axi_1_aw_bits_len;
wire    [2:0]  mem_axi_1_aw_bits_size;
wire    [1:0]  mem_axi_1_aw_bits_burst;
wire           mem_axi_1_aw_bits_lock;
wire    [3:0]  mem_axi_1_aw_bits_cache;
wire    [2:0]  mem_axi_1_aw_bits_prot;
wire    [3:0]  mem_axi_1_aw_bits_qos;
wire           mem_axi_1_w_ready;
wire           mem_axi_1_w_valid;
wire    [63:0] mem_axi_1_w_bits_data;
wire    [7:0]  mem_axi_1_w_bits_strb;
wire           mem_axi_1_w_bits_last;
wire           mem_axi_1_b_ready;
wire           mem_axi_1_b_valid;
wire    [3:0]  mem_axi_1_b_bits_id;
wire    [1:0]  mem_axi_1_b_bits_resp;
wire           mem_axi_1_ar_ready;
wire           mem_axi_1_ar_valid;
wire    [3:0]  mem_axi_1_ar_bits_id;
wire    [35:0] mem_axi_1_ar_bits_addr;
wire    [7:0]  mem_axi_1_ar_bits_len;
wire    [2:0]  mem_axi_1_ar_bits_size;
wire    [1:0]  mem_axi_1_ar_bits_burst;
wire           mem_axi_1_ar_bits_lock;
wire    [3:0]  mem_axi_1_ar_bits_cache;
wire    [2:0]  mem_axi_1_ar_bits_prot;
wire    [3:0]  mem_axi_1_ar_bits_qos;
wire           mem_axi_1_r_ready;
wire           mem_axi_1_r_valid;
wire    [3:0]  mem_axi_1_r_bits_id;
wire    [63:0] mem_axi_1_r_bits_data;
wire    [1:0]  mem_axi_1_r_bits_resp;
wire           mem_axi_1_r_bits_last;

wire        mem_axi_2_aw_ready;
wire        mem_axi_2_aw_valid; 
wire [3:0]  mem_axi_2_aw_bits_id;
wire [27:0] mem_axi_2_aw_bits_addr;
wire [7:0]  mem_axi_2_aw_bits_len;
wire [2:0]  mem_axi_2_aw_bits_size;
wire [1:0]  mem_axi_2_aw_bits_burst;
wire        mem_axi_2_aw_bits_lock;
wire [3:0]  mem_axi_2_aw_bits_cache;
wire [2:0]  mem_axi_2_aw_bits_prot;
wire [3:0]  mem_axi_2_aw_bits_qos;
wire        mem_axi_2_w_ready;
wire        mem_axi_2_w_valid;
wire [63:0] mem_axi_2_w_bits_data;
wire [7:0]  mem_axi_2_w_bits_strb;
wire        mem_axi_2_w_bits_last;
wire        mem_axi_2_b_ready;
wire        mem_axi_2_b_valid;
wire [3:0]  mem_axi_2_b_bits_id;
wire [1:0]  mem_axi_2_b_bits_resp;
wire        mem_axi_2_ar_ready;
wire        mem_axi_2_ar_valid;
wire [3:0]  mem_axi_2_ar_bits_id;
wire [27:0] mem_axi_2_ar_bits_addr;
wire [7:0]  mem_axi_2_ar_bits_len;
wire [2:0]  mem_axi_2_ar_bits_size;
wire [1:0]  mem_axi_2_ar_bits_burst;
wire        mem_axi_2_ar_bits_lock;
wire [3:0]  mem_axi_2_ar_bits_cache;
wire [2:0]  mem_axi_2_ar_bits_prot;
wire [3:0]  mem_axi_2_ar_bits_qos;
wire        mem_axi_2_r_ready;
wire        mem_axi_2_r_valid;
wire [3:0]  mem_axi_2_r_bits_id;
wire [63:0] mem_axi_2_r_bits_data;
wire [1:0]  mem_axi_2_r_bits_resp;
wire        mem_axi_2_r_bits_last;

wire           mmio_axi_0_aw_ready;
wire           mmio_axi_0_aw_valid;
wire    [3:0]  mmio_axi_0_aw_bits_id;
wire    [35:0] mmio_axi_0_aw_bits_addr;
wire    [7:0]  mmio_axi_0_aw_bits_len;
wire    [2:0]  mmio_axi_0_aw_bits_size;
wire    [1:0]  mmio_axi_0_aw_bits_burst;
wire           mmio_axi_0_aw_bits_lock;
wire    [3:0]  mmio_axi_0_aw_bits_cache;
wire    [2:0]  mmio_axi_0_aw_bits_prot;
wire    [3:0]  mmio_axi_0_aw_bits_qos;
wire           mmio_axi_0_w_ready;
wire           mmio_axi_0_w_valid;
wire    [63:0] mmio_axi_0_w_bits_data;
wire    [7:0]  mmio_axi_0_w_bits_strb;
wire           mmio_axi_0_w_bits_last;
wire           mmio_axi_0_b_ready;
wire           mmio_axi_0_b_valid;
wire    [3:0]  mmio_axi_0_b_bits_id;
wire    [1:0]  mmio_axi_0_b_bits_resp;
wire           mmio_axi_0_ar_ready;
wire           mmio_axi_0_ar_valid;
wire    [3:0]  mmio_axi_0_ar_bits_id;
wire    [35:0] mmio_axi_0_ar_bits_addr;
wire    [7:0]  mmio_axi_0_ar_bits_len;
wire    [2:0]  mmio_axi_0_ar_bits_size;
wire    [1:0]  mmio_axi_0_ar_bits_burst;
wire           mmio_axi_0_ar_bits_lock;
wire    [3:0]  mmio_axi_0_ar_bits_cache;
wire    [2:0]  mmio_axi_0_ar_bits_prot;
wire    [3:0]  mmio_axi_0_ar_bits_qos;
wire           mmio_axi_0_r_ready;
wire           mmio_axi_0_r_valid;
wire    [3:0]  mmio_axi_0_r_bits_id;
wire    [63:0] mmio_axi_0_r_bits_data;
wire    [1:0]  mmio_axi_0_r_bits_resp;
wire           mmio_axi_0_r_bits_last;

wire        front_axi_0_aw_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_aw_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_aw_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [36:0] front_axi_0_aw_bits_addr; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [7:0]  front_axi_0_aw_bits_len; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [2:0]  front_axi_0_aw_bits_size; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [1:0]  front_axi_0_aw_bits_burst; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_aw_bits_lock; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [3:0]  front_axi_0_aw_bits_cache; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [2:0]  front_axi_0_aw_bits_prot; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [3:0]  front_axi_0_aw_bits_qos; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_w_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_w_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [63:0] front_axi_0_w_bits_data; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [7:0]  front_axi_0_w_bits_strb; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_w_bits_last; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_b_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_b_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_b_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [1:0]  front_axi_0_b_bits_resp; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_ar_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_ar_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_ar_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [36:0] front_axi_0_ar_bits_addr; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [7:0]  front_axi_0_ar_bits_len; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [2:0]  front_axi_0_ar_bits_size; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [1:0]  front_axi_0_ar_bits_burst; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_ar_bits_lock; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [3:0]  front_axi_0_ar_bits_cache; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [2:0]  front_axi_0_ar_bits_prot; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [3:0]  front_axi_0_ar_bits_qos; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_r_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_r_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_r_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [63:0] front_axi_0_r_bits_data; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire [1:0]  front_axi_0_r_bits_resp; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_0_r_bits_last; // @[:freedom.serve.SERVEiFPGAConfig.fir@503055.4]
wire        front_axi_1_aw_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_aw_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_aw_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [36:0] front_axi_1_aw_bits_addr; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [7:0]  front_axi_1_aw_bits_len; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [2:0]  front_axi_1_aw_bits_size; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [1:0]  front_axi_1_aw_bits_burst; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_aw_bits_lock; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [3:0]  front_axi_1_aw_bits_cache; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [2:0]  front_axi_1_aw_bits_prot; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [3:0]  front_axi_1_aw_bits_qos; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_w_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_w_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [63:0] front_axi_1_w_bits_data; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [7:0]  front_axi_1_w_bits_strb; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_w_bits_last; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_b_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_b_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_b_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [1:0]  front_axi_1_b_bits_resp; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_ar_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_ar_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_ar_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [36:0] front_axi_1_ar_bits_addr; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [7:0]  front_axi_1_ar_bits_len; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [2:0]  front_axi_1_ar_bits_size; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [1:0]  front_axi_1_ar_bits_burst; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_ar_bits_lock; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [3:0]  front_axi_1_ar_bits_cache; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [2:0]  front_axi_1_ar_bits_prot; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [3:0]  front_axi_1_ar_bits_qos; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_r_ready; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_r_valid; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_r_bits_id; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [63:0] front_axi_1_r_bits_data; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire [1:0]  front_axi_1_r_bits_resp; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]
wire        front_axi_1_r_bits_last; // @[:freedom.serve.SERVEiFPGAConfig.fir@503056.4]

// wire [32:0] hbm_axi_araddr;
// wire [1:0]  hbm_axi_arburst;
// wire [3:0]  hbm_axi_arcache;
// wire [7:0]  hbm_axi_arlen;
// wire [0:0]  hbm_axi_arlock;
// wire [2:0]  hbm_axi_arprot;
// wire [3:0]  hbm_axi_arqos;
// wire        hbm_axi_arready;
// wire [3:0]  hbm_axi_arregion;
// wire [2:0]  hbm_axi_arsize;
// wire        hbm_axi_arvalid;
// wire [32:0] hbm_axi_awaddr;
// wire [1:0]  hbm_axi_awburst;
// wire [3:0]  hbm_axi_awcache;
// wire [7:0]  hbm_axi_awlen;
// wire [0:0]  hbm_axi_awlock;
// wire [2:0]  hbm_axi_awprot;
// wire [3:0]  hbm_axi_awqos;
// wire        hbm_axi_awready;
// wire [3:0]  hbm_axi_awregion;
// wire [2:0]  hbm_axi_awsize;
// wire        hbm_axi_awvalid;
// wire        hbm_axi_bready;
// wire [1:0]  hbm_axi_bresp;
// wire        hbm_axi_bvalid;
// wire [255:0]hbm_axi_rdata;
// wire        hbm_axi_rlast;
// wire        hbm_axi_rready;
// wire [1:0]  hbm_axi_rresp;
// wire        hbm_axi_rvalid;
// wire [255:0]hbm_axi_wdata;
// wire        hbm_axi_wlast;
// wire        hbm_axi_wready;
// wire [31:0] hbm_axi_wstrb;
// wire        hbm_axi_wvalid;
// wire [32:0] hbm_axi_1_araddr;
// wire [1:0]  hbm_axi_1_arburst;
// wire [3:0]  hbm_axi_1_arcache;
// wire [7:0]  hbm_axi_1_arlen;
// wire [0:0]  hbm_axi_1_arlock;
// wire [2:0]  hbm_axi_1_arprot;
// wire [3:0]  hbm_axi_1_arqos;
// wire        hbm_axi_1_arready;
// wire [3:0]  hbm_axi_1_arregion;
// wire [2:0]  hbm_axi_1_arsize;
// wire        hbm_axi_1_arvalid;
// wire [32:0] hbm_axi_1_awaddr;
// wire [1:0]  hbm_axi_1_awburst;
// wire [3:0]  hbm_axi_1_awcache;
// wire [7:0]  hbm_axi_1_awlen;
// wire [0:0]  hbm_axi_1_awlock;
// wire [2:0]  hbm_axi_1_awprot;
// wire [3:0]  hbm_axi_1_awqos;
// wire        hbm_axi_1_awready;
// wire [3:0]  hbm_axi_1_awregion;
// wire [2:0]  hbm_axi_1_awsize;
// wire        hbm_axi_1_awvalid;
// wire        hbm_axi_1_bready;
// wire [1:0]  hbm_axi_1_bresp;
// wire        hbm_axi_1_bvalid;
// wire [255:0]hbm_axi_1_rdata;
// wire        hbm_axi_1_rlast;
// wire        hbm_axi_1_rready;
// wire [1:0]  hbm_axi_1_rresp;
// wire        hbm_axi_1_rvalid;
// wire [255:0]hbm_axi_1_wdata;
// wire        hbm_axi_1_wlast;
// wire        hbm_axi_1_wready;
// wire [31:0] hbm_axi_1_wstrb;
// wire        hbm_axi_1_wvalid;
// wire [0:0]  hbm_resetn;
wire        DDR_CLK;
wire        c0_ddr4_ui_clk_sync_rst;
wire        c0_ddr4_ui_clk;

wire        apb_complete_0;
wire        apb_complete_1;

wire             c0_ddr4_aresetn;
wire [3:0]      c0_ddr4_s_axi_awid;
wire [31:0]    c0_ddr4_s_axi_awaddr;
wire [7:0]                       c0_ddr4_s_axi_awlen;
wire [2:0]                       c0_ddr4_s_axi_awsize;
wire [1:0]                       c0_ddr4_s_axi_awburst;
wire [0:0]                       c0_ddr4_s_axi_awlock;
wire [3:0]                       c0_ddr4_s_axi_awcache;
wire [2:0]                       c0_ddr4_s_axi_awprot;
wire [3:0]                       c0_ddr4_s_axi_awqos;
wire                             c0_ddr4_s_axi_awvalid;
wire                             c0_ddr4_s_axi_awready;
   // Slave Interface Write Data Ports
wire [511:0]    c0_ddr4_s_axi_wdata;
wire [63:0]  c0_ddr4_s_axi_wstrb;
wire                             c0_ddr4_s_axi_wlast;
wire                             c0_ddr4_s_axi_wvalid;
wire                             c0_ddr4_s_axi_wready;
//wireve Interface Write Response Ports
wire                             c0_ddr4_s_axi_bready;
wire [3:0]      c0_ddr4_s_axi_bid;
wire [1:0]                       c0_ddr4_s_axi_bresp;
wire                             c0_ddr4_s_axi_bvalid;
//wireve Interface Read Address Ports
wire [3:0]      c0_ddr4_s_axi_arid;
wire [31:0]    c0_ddr4_s_axi_araddr;
wire [7:0]                       c0_ddr4_s_axi_arlen;
wire [2:0]                       c0_ddr4_s_axi_arsize;
wire [1:0]                       c0_ddr4_s_axi_arburst;
wire [0:0]                       c0_ddr4_s_axi_arlock;
wire [3:0]                       c0_ddr4_s_axi_arcache;
wire [2:0]                       c0_ddr4_s_axi_arprot;
wire [3:0]                       c0_ddr4_s_axi_arqos;
wire                             c0_ddr4_s_axi_arvalid;
wire                             c0_ddr4_s_axi_arready;
//wireve Interface Read Data Ports
wire                             c0_ddr4_s_axi_rready;
wire [3:0]      c0_ddr4_s_axi_rid;
wire [511:0]    c0_ddr4_s_axi_rdata;
wire [1:0]                       c0_ddr4_s_axi_rresp;
wire                             c0_ddr4_s_axi_rlast;
wire                             c0_ddr4_s_axi_rvalid;

wire [35:0]APB_M_0_paddr;
wire APB_M_0_penable;
wire [31:0]APB_M_0_prdata;
wire [0:0]APB_M_0_pready;
wire [0:0]APB_M_0_psel;
wire [0:0]APB_M_0_pslverr;
wire [31:0]APB_M_0_pwdata;
wire APB_M_0_pwrite;
/*
`ifndef _WITH_DDR_
reg [2:0]        apb_state_0;
reg [2:0]        apb_state_1;
 reg [4:0]        ext_reset;

always @(posedge cpu_clk)
    begin
        if(apb_resetn == 1'b0)
             apb_state_0 <= 3'b001;
        else if(apb_state_0[0])// && apb_complete_0 == 1'b0)
            apb_state_0 <= 3'b010;
        else if(apb_state_0[1] && apb_complete_0 == 1'b1)
            apb_state_0 <= 3'b100;
        else if(apb_state_0[2] )//&& apb_state_1[2])
            apb_state_0 <= 3'b000;
        else
            apb_state_0 <= apb_state_0;
    end

always @(posedge cpu_clk)
    begin
        if(apb_resetn == 1'b0)
             apb_state_1 <= 3'b001;
        else if(apb_state_1[0] && apb_complete_1 == 1'b0)
            apb_state_1 <= 3'b010;
        else if(apb_state_1[1] && apb_complete_1 == 1'b1)
            apb_state_1 <= 3'b100;
        else if(apb_state_1[2] && apb_state_0[2])
            apb_state_1 <= 3'b000;
        else
            apb_state_1 <= apb_state_1;
    end
always @(posedge cpu_clk)
    begin
        ext_reset[0] <=  apb_state_0[2];
        ext_reset[1] <= ext_reset[0];
        ext_reset[2] <= ext_reset[1];
        ext_reset[3] <= ext_reset[2];
        ext_reset[4] <= ext_reset[3];
    end
`endif
*/
assign led[0] = cpu_reset | ~ddr4_init_copmlete;
assign led[1] = uart_int;
assign led[2] = flash_int;

assign front_axi_0_aw_bits_id = 'd0;
assign front_axi_0_aw_bits_lock = 'd0;
assign front_axi_0_aw_bits_qos = 'd0;
assign front_axi_0_ar_bits_id = 'd0;
assign front_axi_0_ar_bits_lock = 'd0;
assign front_axi_0_ar_bits_qos = 'd0;

assign front_axi_1_aw_bits_qos = 'd0;
assign front_axi_1_ar_bits_id = 'd0;
assign front_axi_1_ar_bits_qos = 'd0;
assign front_axi_1_aw_bits_id = 'd0;
assign front_axi_1_aw_bits_lock = 'd0;
assign front_axi_1_ar_bits_lock = 'd0;

top_wrapper top_wrapper(

    .M_AXI_SG_araddr                (front_axi_0_ar_bits_addr),
    .M_AXI_SG_arburst               (front_axi_0_ar_bits_burst),
    .M_AXI_SG_arcache               (front_axi_0_ar_bits_cache),
    .M_AXI_SG_arlen                 (front_axi_0_ar_bits_len),
    .M_AXI_SG_arprot                (front_axi_0_ar_bits_prot),
    .M_AXI_SG_arready               (front_axi_0_ar_ready),
    .M_AXI_SG_arsize                (front_axi_0_ar_bits_size),
    .M_AXI_SG_arvalid               (front_axi_0_ar_valid),
    .M_AXI_SG_rdata                 (front_axi_0_r_bits_data),
    .M_AXI_SG_rlast                 (front_axi_0_r_bits_last),
    .M_AXI_SG_rready                (front_axi_0_r_ready),
    .M_AXI_SG_rresp                 (front_axi_0_r_bits_resp),
    .M_AXI_SG_rvalid                (front_axi_0_r_valid),
    .M_AXI_SG_awaddr                (front_axi_0_aw_bits_addr),
    .M_AXI_SG_awburst               (front_axi_0_aw_bits_burst),
    .M_AXI_SG_awcache               (front_axi_0_aw_bits_cache),
    .M_AXI_SG_awlen                 (front_axi_0_aw_bits_len),
    .M_AXI_SG_awprot                (front_axi_0_aw_bits_prot),
    .M_AXI_SG_awready               (front_axi_0_aw_ready),
    .M_AXI_SG_awsize                (front_axi_0_aw_bits_size),
    .M_AXI_SG_awvalid               (front_axi_0_aw_valid),
    .M_AXI_SG_bready                (front_axi_0_b_ready),
    .M_AXI_SG_bresp                 (front_axi_0_b_bits_resp),
    .M_AXI_SG_bvalid                (front_axi_0_b_valid),
    .M_AXI_SG_wdata                 (front_axi_0_w_bits_data),
    .M_AXI_SG_wlast                 (front_axi_0_w_bits_last),
    .M_AXI_SG_wready                (front_axi_0_w_ready),
    .M_AXI_SG_wstrb                 (front_axi_0_w_bits_strb),
    .M_AXI_SG_wvalid                (front_axi_0_w_valid),

//    .DMA_MM2S_araddr                (front_axi_0_ar_bits_addr),
//    .DMA_MM2S_arburst               (front_axi_0_ar_bits_burst),
//    .DMA_MM2S_arcache               (front_axi_0_ar_bits_cache),
//    .DMA_MM2S_arlen                 (front_axi_0_ar_bits_len),
//    .DMA_MM2S_arprot                (front_axi_0_ar_bits_prot),
//    .DMA_MM2S_arready               (front_axi_0_ar_ready),
//    .DMA_MM2S_arsize                (front_axi_0_ar_bits_size),
//    .DMA_MM2S_arvalid               (front_axi_0_ar_valid),
//    .DMA_MM2S_rdata                 (front_axi_0_r_bits_data),
//    .DMA_MM2S_rlast                 (front_axi_0_r_bits_last),
//    .DMA_MM2S_rready                (front_axi_0_r_ready),
//    .DMA_MM2S_rresp                 (front_axi_0_r_bits_resp),
//    .DMA_MM2S_rvalid                (front_axi_0_r_valid),
//    .DMA_S2MM_awaddr                (front_axi_0_aw_bits_addr),
//    .DMA_S2MM_awburst               (front_axi_0_aw_bits_burst),
//    .DMA_S2MM_awcache               (front_axi_0_aw_bits_cache),
//    .DMA_S2MM_awlen                 (front_axi_0_aw_bits_len),
//    .DMA_S2MM_awprot                (front_axi_0_aw_bits_prot),
//    .DMA_S2MM_awready               (front_axi_0_aw_ready),
//    .DMA_S2MM_awsize                (front_axi_0_aw_bits_size),
//    .DMA_S2MM_awvalid               (front_axi_0_aw_valid),
//    .DMA_S2MM_bready                (front_axi_0_b_ready),
//    .DMA_S2MM_bresp                 (front_axi_0_b_bits_resp),
//    .DMA_S2MM_bvalid                (front_axi_0_b_valid),
//    .DMA_S2MM_wdata                 (front_axi_0_w_bits_data),
//    .DMA_S2MM_wlast                 (front_axi_0_w_bits_last),
//    .DMA_S2MM_wready                (front_axi_0_w_ready),
//    .DMA_S2MM_wstrb                 (front_axi_0_w_bits_strb),
//    .DMA_S2MM_wvalid                (front_axi_0_w_valid),

    .M_AXI_MM2S_araddr                (front_axi_1_ar_bits_addr),
    .M_AXI_MM2S_arburst               (front_axi_1_ar_bits_burst),
    .M_AXI_MM2S_arcache               (front_axi_1_ar_bits_cache),
    .M_AXI_MM2S_arlen                 (front_axi_1_ar_bits_len),
    .M_AXI_MM2S_arprot                (front_axi_1_ar_bits_prot),
    .M_AXI_MM2S_arready               (front_axi_1_ar_ready),
    .M_AXI_MM2S_arsize                (front_axi_1_ar_bits_size),
    .M_AXI_MM2S_arvalid               (front_axi_1_ar_valid),
    .M_AXI_MM2S_rdata                 (front_axi_1_r_bits_data),
    .M_AXI_MM2S_rlast                 (front_axi_1_r_bits_last),
    .M_AXI_MM2S_rready                (front_axi_1_r_ready),
    .M_AXI_MM2S_rresp                 (front_axi_1_r_bits_resp),
    .M_AXI_MM2S_rvalid                (front_axi_1_r_valid),
    .M_AXI_S2MM_awaddr                (front_axi_1_aw_bits_addr),
    .M_AXI_S2MM_awburst               (front_axi_1_aw_bits_burst),
    .M_AXI_S2MM_awcache               (front_axi_1_aw_bits_cache),
    .M_AXI_S2MM_awlen                 (front_axi_1_aw_bits_len),
    .M_AXI_S2MM_awprot                (front_axi_1_aw_bits_prot),
    .M_AXI_S2MM_awready               (front_axi_1_aw_ready),
    .M_AXI_S2MM_awsize                (front_axi_1_aw_bits_size),
    .M_AXI_S2MM_awvalid               (front_axi_1_aw_valid),
    .M_AXI_S2MM_bready                (front_axi_1_b_ready),
    .M_AXI_S2MM_bresp                 (front_axi_1_b_bits_resp),
    .M_AXI_S2MM_bvalid                (front_axi_1_b_valid),
    .M_AXI_S2MM_wdata                 (front_axi_1_w_bits_data),
    .M_AXI_S2MM_wlast                 (front_axi_1_w_bits_last),
    .M_AXI_S2MM_wready                (front_axi_1_w_ready),
    .M_AXI_S2MM_wstrb                 (front_axi_1_w_bits_strb),
    .M_AXI_S2MM_wvalid                (front_axi_1_w_valid),
//   .MM2S_S2MM_arid  (front_axi_1_ar_bits_id),
//   .MM2S_S2MM_awid  (front_axi_1_aw_bits_id),
//   .MM2S_S2MM_bid  (front_axi_1_b_bits_id),
//   .MM2S_S2MM_rid  (front_axi_1_r_bits_id),
//   .MM2S_S2MM_awlock  (front_axi_1_aw_bits_lock),
//   .MM2S_S2MM_arlock  (front_axi_1_ar_bits_lock),
//   .MM2S_S2MM_awqos  (front_axi_1_aw_bits_qos),
//   .MM2S_S2MM_arqos  (front_axi_1_ar_bits_qos),

//   .MM2S_S2MM_araddr                  (front_axi_1_ar_bits_addr),
//   .MM2S_S2MM_arburst                 (front_axi_1_ar_bits_burst),
//   .MM2S_S2MM_arcache                 (front_axi_1_ar_bits_cache),
//   .MM2S_S2MM_arlen                   (front_axi_1_ar_bits_len),
//   .MM2S_S2MM_arprot                  (front_axi_1_ar_bits_prot),
//   .MM2S_S2MM_arready                 (front_axi_1_ar_ready),
//   .MM2S_S2MM_arsize                  (front_axi_1_ar_bits_size),
//   .MM2S_S2MM_arvalid                 (front_axi_1_ar_valid),
//   .MM2S_S2MM_awaddr                  (front_axi_1_aw_bits_addr),
//   .MM2S_S2MM_awburst                 (front_axi_1_aw_bits_burst),
//   .MM2S_S2MM_awcache                 (front_axi_1_aw_bits_cache),
//   .MM2S_S2MM_awlen                   (front_axi_1_aw_bits_len),
//   .MM2S_S2MM_awprot                  (front_axi_1_aw_bits_prot),
//   .MM2S_S2MM_awready                 (front_axi_1_aw_ready),
//   .MM2S_S2MM_awsize                  (front_axi_1_aw_bits_size),
//   .MM2S_S2MM_awvalid                 (front_axi_1_aw_valid),
//   .MM2S_S2MM_bready                  (front_axi_1_b_ready),
//   .MM2S_S2MM_bresp                   (front_axi_1_b_bits_resp),
//   .MM2S_S2MM_bvalid                  (front_axi_1_b_valid),
//   .MM2S_S2MM_rdata                   (front_axi_1_r_bits_data),
//   .MM2S_S2MM_rlast                   (front_axi_1_r_bits_last),
//   .MM2S_S2MM_rready                  (front_axi_1_r_ready),
//   .MM2S_S2MM_rresp                   (front_axi_1_r_bits_resp),
//   .MM2S_S2MM_rvalid                  (front_axi_1_r_valid),
//   .MM2S_S2MM_wdata                   (front_axi_1_w_bits_data),
//   .MM2S_S2MM_wlast                   (front_axi_1_w_bits_last),
//   .MM2S_S2MM_wready                  (front_axi_1_w_ready),
//   .MM2S_S2MM_wstrb                   (front_axi_1_w_bits_strb),
//   .MM2S_S2MM_wvalid                  (front_axi_1_w_valid),


    
    .mdio_mdc_mdc                   (mdc),
    .mdio_mdc_mdio_io               (mdio),   
    //.phy_rst_n_0                    (phy_rst_n),
    .sgmii_lvds_rxn                 (sgmii_lvds_rxn),
    .sgmii_lvds_rxp                 (sgmii_lvds_rxp),
    .sgmii_lvds_txn                 (sgmii_lvds_txn),
    .sgmii_lvds_txp                 (sgmii_lvds_txp),
    .sgmii_phyclk_clk_n             (sgmii_phyclk_clk_n),
    .sgmii_phyclk_clk_p             (sgmii_phyclk_clk_p),
    .signal_detect_0                (1'b1),
    .dummy_port_in                  (dummy_port_in),

    /*.SPI_0_io0_io                   (SPI_0_io0_io),
    .SPI_0_io1_io                   (SPI_0_io1_io),
    .SPI_0_io2_io                   (SPI_0_io2_io),
    .SPI_0_io3_io                   (SPI_0_io3_io),
    .SPI_0_sck_io                   (SPI_0_sck_io),
    .SPI_0_ss_io                    (SPI_0_ss_io),*/
    
    .cpu_reset                      (cpu_reset),
`ifdef _WITH_DDR_
    .DDR_port_resetn                (~c0_ddr4_ui_clk_sync_rst),
       .ddr4_sdram_act_n               (ddr4_sdram_act_n),
   .ddr4_sdram_adr                 (ddr4_sdram_adr),
   .ddr4_sdram_ba                  (ddr4_sdram_ba),
   .ddr4_sdram_bg                  (ddr4_sdram_bg),
   .ddr4_sdram_cke                 (ddr4_sdram_cke),
   .ddr4_sdram_odt                 (ddr4_sdram_odt),
   .ddr4_sdram_cs_n                (ddr4_sdram_cs_n),
   .ddr4_sdram_ck_t                (ddr4_sdram_ck_t),
   .ddr4_sdram_ck_c                (ddr4_sdram_ck_c),
   .ddr4_sdram_reset_n             (ddr4_sdram_reset_n),
   .ddr4_sdram_dm_n            (ddr4_sdram_dm_n),
   .ddr4_sdram_dq                  (ddr4_sdram_dq),
   .ddr4_sdram_dqs_c               (ddr4_sdram_dqs_c),
   .ddr4_sdram_dqs_t               (ddr4_sdram_dqs_t),
    //.default_100mhz_clk_clk_n       (default_100mhz_clk_clk_n),
    //.default_100mhz_clk_clk_p       (default_100mhz_clk_clk_p),
    .c0_sys_clk_i_0                 (RefCLK),
   //.c0_init_calib_complete      (),
   //.c0_ddr4_ui_clk              (c0_ddr4_ui_clk),
   .c0_ddr4_ui_clk_sync_rst_0     (c0_ddr4_ui_clk_sync_rst),
    //.DDR_SYS_CLK_clk_p                  (DDR_SYS_CLK_p),
    //.DDR_SYS_CLK_clk_n                  (DDR_SYS_CLK_n),    
    //.hbm_clk                       (hbm_apb_clk),
    .cpu_clk_i                        (cpu_clk),
    .ext_spi_clk                    (spi_clk),
    .sys_rst                        (reset),
    //.apb_resetn                     (apb_resetn),
`else
    .cpu_clk_i                      (cpu_clk),
    .ext_spi_clk                    (spi_clk),
    .sys_rst                        (reset),
`endif
    .eth_interrupt                  (mac_int),
    .s2mm_introut                   (s2mm_introut),
    .mm2s_introut                   (mm2s_introut),
    .interrupt_0                    (uart_int),
    .ip2intc_irpt_0                 (flash_int),
    .hbm_resetn                     (0),
    .rs232_uart_0_rxd               (rs232_uart_0_rxd),
    .rs232_uart_0_txd               (rs232_uart_0_txd),
    
    //.APB_M_0_paddr                  (APB_M_0_paddr),
    //.APB_M_0_penable                (APB_M_0_penable),
    //.APB_M_0_prdata                 (APB_M_0_prdata),
    //.APB_M_0_pready                 (APB_M_0_pready),
    //.APB_M_0_psel                   (APB_M_0_psel),
    //.APB_M_0_pslverr                (APB_M_0_pslverr),
    //.APB_M_0_pwdata                 (APB_M_0_pwdata),
    //.APB_M_0_pwrite                 (APB_M_0_pwrite),

    .mem0_S_araddr                  (mem_axi_0_ar_bits_addr),
    .mem0_S_arburst                 (mem_axi_0_ar_bits_burst),
    .mem0_S_arcache                 (mem_axi_0_ar_bits_cache),
    .mem0_S_arid                    (mem_axi_0_ar_bits_id),
    .mem0_S_arlen                   (mem_axi_0_ar_bits_len),
    .mem0_S_arlock                  (mem_axi_0_ar_bits_lock),
    .mem0_S_arprot                  (mem_axi_0_ar_bits_prot),
    .mem0_S_arqos                   (mem_axi_0_ar_bits_qos),
    .mem0_S_arready                 (mem_axi_0_ar_ready),
//    .mem0_S_arregion                ('d0),
    .mem0_S_arsize                  (mem_axi_0_ar_bits_size),
    .mem0_S_arvalid                 (mem_axi_0_ar_valid),
    .mem0_S_awaddr                  (mem_axi_0_aw_bits_addr),
    .mem0_S_awburst                 (mem_axi_0_aw_bits_burst),
    .mem0_S_awcache                 (mem_axi_0_aw_bits_cache),
    .mem0_S_awid                    (mem_axi_0_aw_bits_id),
    .mem0_S_awlen                   (mem_axi_0_aw_bits_len),
    .mem0_S_awlock                  (mem_axi_0_aw_bits_lock),
    .mem0_S_awprot                  (mem_axi_0_aw_bits_prot),
    .mem0_S_awqos                   (mem_axi_0_aw_bits_qos),
    .mem0_S_awready                 (mem_axi_0_aw_ready),
//    .mem0_S_awregion                ('d0),
    .mem0_S_awsize                  (mem_axi_0_aw_bits_size),
    .mem0_S_awvalid                 (mem_axi_0_aw_valid),
    .mem0_S_bid                     (mem_axi_0_b_bits_id),
    .mem0_S_bready                  (mem_axi_0_b_ready),
    .mem0_S_bresp                   (mem_axi_0_b_bits_resp),
    .mem0_S_bvalid                  (mem_axi_0_b_valid),
    .mem0_S_rdata                   (mem_axi_0_r_bits_data),
    .mem0_S_rid                     (mem_axi_0_r_bits_id),
    .mem0_S_rlast                   (mem_axi_0_r_bits_last),
    .mem0_S_rready                  (mem_axi_0_r_ready),
    .mem0_S_rresp                   (mem_axi_0_r_bits_resp),
    .mem0_S_rvalid                  (mem_axi_0_r_valid),
    .mem0_S_wdata                   (mem_axi_0_w_bits_data),
    .mem0_S_wlast                   (mem_axi_0_w_bits_last),
    .mem0_S_wready                  (mem_axi_0_w_ready),
    .mem0_S_wstrb                   (mem_axi_0_w_bits_strb),
    .mem0_S_wvalid                  (mem_axi_0_w_valid),

    .mem_S_araddr                   (mem_axi_1_ar_bits_addr),
    .mem_S_arburst                  (mem_axi_1_ar_bits_burst),
    .mem_S_arid                     (mem_axi_1_ar_bits_id),
    .mem_S_arcache                  (mem_axi_1_ar_bits_cache),
    .mem_S_arlen                    (mem_axi_1_ar_bits_len),
    .mem_S_arlock                   (mem_axi_1_ar_bits_lock),
    .mem_S_arprot                   (mem_axi_1_ar_bits_prot),
    .mem_S_arqos                    (mem_axi_1_ar_bits_qos),
    .mem_S_arready                  (mem_axi_1_ar_ready),
    .mem_S_arsize                   (mem_axi_1_ar_bits_size),
    .mem_S_arvalid                  (mem_axi_1_ar_valid),
    .mem_S_awid                     (mem_axi_1_aw_bits_id),
    .mem_S_awaddr                   (mem_axi_1_aw_bits_addr),
    .mem_S_awburst                  (mem_axi_1_aw_bits_burst),
    .mem_S_awcache                  (mem_axi_1_aw_bits_cache),
    .mem_S_awlen                    (mem_axi_1_aw_bits_len),
    .mem_S_awlock                   (mem_axi_1_aw_bits_lock),
    .mem_S_awprot                   (mem_axi_1_aw_bits_prot),
    .mem_S_awqos                    (mem_axi_1_aw_bits_qos),
    .mem_S_awready                  (mem_axi_1_aw_ready),
    .mem_S_awsize                   (mem_axi_1_aw_bits_size),
    .mem_S_awvalid                  (mem_axi_1_aw_valid),
    .mem_S_bready                   (mem_axi_1_b_ready),
    .mem_S_bid                      (mem_axi_1_b_bits_id),
    .mem_S_bresp                    (mem_axi_1_b_bits_resp),
    .mem_S_bvalid                   (mem_axi_1_b_valid),
    .mem_S_rdata                    (mem_axi_1_r_bits_data),
    .mem_S_rid                      (mem_axi_1_r_bits_id),
    .mem_S_rlast                    (mem_axi_1_r_bits_last),
    .mem_S_rready                   (mem_axi_1_r_ready),
    .mem_S_rresp                    (mem_axi_1_r_bits_resp),
    .mem_S_rvalid                   (mem_axi_1_r_valid),
    .mem_S_wdata                    (mem_axi_1_w_bits_data),
    .mem_S_wlast                    (mem_axi_1_w_bits_last),
    .mem_S_wready                   (mem_axi_1_w_ready),
    .mem_S_wstrb                    (mem_axi_1_w_bits_strb),
    .mem_S_wvalid                   (mem_axi_1_w_valid),

    .mem2_S_araddr                   (mem_axi_2_ar_bits_addr),
    .mem2_S_arburst                  (mem_axi_2_ar_bits_burst),
    .mem2_S_arid                     (mem_axi_2_ar_bits_id),
    .mem2_S_arcache                  (4'b0011),//(mem_axi_2_ar_bits_cache),
    .mem2_S_arlen                    (mem_axi_2_ar_bits_len),
    .mem2_S_arlock                   (mem_axi_2_ar_bits_lock),
    .mem2_S_arprot                   (mem_axi_2_ar_bits_prot),
    .mem2_S_arqos                    (mem_axi_2_ar_bits_qos),
    .mem2_S_arready                  (mem_axi_2_ar_ready),
    .mem2_S_arsize                   (mem_axi_2_ar_bits_size),
    .mem2_S_arvalid                  (mem_axi_2_ar_valid),
    .mem2_S_awid                     (mem_axi_2_aw_bits_id),
    .mem2_S_awaddr                   (mem_axi_2_aw_bits_addr),
    .mem2_S_awburst                  (mem_axi_2_aw_bits_burst),
    .mem2_S_awcache                  (4'b0011),//(mem_axi_2_aw_bits_cache),
    .mem2_S_awlen                    (mem_axi_2_aw_bits_len),
    .mem2_S_awlock                   (mem_axi_2_aw_bits_lock),
    .mem2_S_awprot                   (mem_axi_2_aw_bits_prot),
    .mem2_S_awqos                    (mem_axi_2_aw_bits_qos),
    .mem2_S_awready                  (mem_axi_2_aw_ready),
    .mem2_S_awsize                   (mem_axi_2_aw_bits_size),
    .mem2_S_awvalid                  (mem_axi_2_aw_valid),
    .mem2_S_bready                   (mem_axi_2_b_ready),
    .mem2_S_bid                      (mem_axi_2_b_bits_id),
    .mem2_S_bresp                    (mem_axi_2_b_bits_resp),
    .mem2_S_bvalid                   (mem_axi_2_b_valid),
    .mem2_S_rdata                    (mem_axi_2_r_bits_data),
    .mem2_S_rid                      (mem_axi_2_r_bits_id),
    .mem2_S_rlast                    (mem_axi_2_r_bits_last),
    .mem2_S_rready                   (mem_axi_2_r_ready),
    .mem2_S_rresp                    (mem_axi_2_r_bits_resp),
    .mem2_S_rvalid                   (mem_axi_2_r_valid),
    .mem2_S_wdata                    (mem_axi_2_w_bits_data),
    .mem2_S_wlast                    (mem_axi_2_w_bits_last),
    .mem2_S_wready                   (mem_axi_2_w_ready),
    .mem2_S_wstrb                    (mem_axi_2_w_bits_strb),
    .mem2_S_wvalid                   (mem_axi_2_w_valid),

    .mmio_S_arid                    (mmio_axi_0_ar_bits_id),
    .mmio_S_araddr                  (mmio_axi_0_ar_bits_addr),
    .mmio_S_arburst                 (mmio_axi_0_ar_bits_burst),
    .mmio_S_arcache                 (mmio_axi_0_ar_bits_cache),
    .mmio_S_arlen                   (mmio_axi_0_ar_bits_len),
    .mmio_S_arlock                  (mmio_axi_0_ar_bits_lock),
    .mmio_S_arprot                  (mmio_axi_0_ar_bits_prot),
    .mmio_S_arqos                   (mmio_axi_0_ar_bits_qos),
    .mmio_S_arready                 (mmio_axi_0_ar_ready),
    .mmio_S_arsize                  (mmio_axi_0_ar_bits_size),
    .mmio_S_arvalid                 (mmio_axi_0_ar_valid),
    .mmio_S_awaddr                  (mmio_axi_0_aw_bits_addr),
    .mmio_S_awburst                 (mmio_axi_0_aw_bits_burst),
    .mmio_S_awcache                 (mmio_axi_0_aw_bits_cache),
    .mmio_S_awid                    (mmio_axi_0_aw_bits_id),
    .mmio_S_awlen                   (mmio_axi_0_aw_bits_len),
    .mmio_S_awlock                  (mmio_axi_0_aw_bits_lock),
    .mmio_S_awprot                  (mmio_axi_0_aw_bits_prot),
    .mmio_S_awqos                   (mmio_axi_0_aw_bits_qos),
    .mmio_S_awready                 (mmio_axi_0_aw_ready),
    .mmio_S_awsize                  (mmio_axi_0_aw_bits_size),
    .mmio_S_awvalid                 (mmio_axi_0_aw_valid),
    .mmio_S_bready                  (mmio_axi_0_b_ready),
    .mmio_S_bresp                   (mmio_axi_0_b_bits_resp),
    .mmio_S_bid                     (mmio_axi_0_b_bits_id),
    .mmio_S_bvalid                  (mmio_axi_0_b_valid),
    .mmio_S_rdata                   (mmio_axi_0_r_bits_data),
    .mmio_S_rlast                   (mmio_axi_0_r_bits_last),
    .mmio_S_rid                     (mmio_axi_0_r_bits_id),
    .mmio_S_rready                  (mmio_axi_0_r_ready),
    .mmio_S_rresp                   (mmio_axi_0_r_bits_resp),
    .mmio_S_rvalid                  (mmio_axi_0_r_valid),
    .mmio_S_wdata                   (mmio_axi_0_w_bits_data),
    .mmio_S_wlast                   (mmio_axi_0_w_bits_last),
    .mmio_S_wready                  (mmio_axi_0_w_ready),
    .mmio_S_wstrb                   (mmio_axi_0_w_bits_strb),
    .mmio_S_wvalid                  (mmio_axi_0_w_valid),

    .hbm_axi_araddr                 (0),
    .hbm_axi_arburst                (0),
    .hbm_axi_arcache                (),
    .hbm_axi_arlen                  (0),
    .hbm_axi_arlock                 (),
    .hbm_axi_arprot                 (),
    .hbm_axi_arqos                  () ,
    .hbm_axi_arready                (0),
    .hbm_axi_arregion               (),
    .hbm_axi_arsize                 (0),
    .hbm_axi_arvalid                (0),
    .hbm_axi_awaddr                 (0),
    .hbm_axi_awburst                (0),
    .hbm_axi_awcache                (),
    .hbm_axi_awlen                  (0),
    .hbm_axi_awlock                 (),
    .hbm_axi_awprot                 (),
    .hbm_axi_awqos                  (),
    .hbm_axi_awready                (0),
    .hbm_axi_awregion               (),
    .hbm_axi_awsize                 (0),
    .hbm_axi_awvalid                (0),
    .hbm_axi_bready                 (0),
    .hbm_axi_bresp                  (0),
    .hbm_axi_bvalid                 (0),
    .hbm_axi_rdata                  (0),
    .hbm_axi_rlast                  (0),
    .hbm_axi_rready                 (0),
    .hbm_axi_rresp                  (0),
    .hbm_axi_rvalid                 (0),
    .hbm_axi_wdata                  (0),
    .hbm_axi_wlast                  (0),
    .hbm_axi_wready                 (0),
    .hbm_axi_wstrb                  (0),
    .hbm_axi_wvalid                 (0)

`ifndef  _WITH_DDR_
   ,.hbm_axi_1_araddr                 (0),
    .hbm_axi_1_arburst                (0),
    .hbm_axi_1_arlen                  (0),
    .hbm_axi_1_arready                (0),
    .hbm_axi_1_arsize                 (0),
    .hbm_axi_1_arvalid                (0),
    .hbm_axi_1_awaddr                 (0),
    .hbm_axi_1_awburst                (0),
    .hbm_axi_1_awlen                  (0),
    .hbm_axi_1_awready                (0),
    .hbm_axi_1_awsize                 (0),
    .hbm_axi_1_awvalid                (0),
    .hbm_axi_1_bready                 (0),
    .hbm_axi_1_bresp                  (0),
    .hbm_axi_1_bvalid                 (0),
    .hbm_axi_1_rdata                  (0),
    .hbm_axi_1_rlast                  (0),
    .hbm_axi_1_rready                 (0),
    .hbm_axi_1_rresp                  (0),
    .hbm_axi_1_rvalid                 (0),
    .hbm_axi_1_wdata                  (0),
    .hbm_axi_1_wlast                  (0),
    .hbm_axi_1_wready                 (0),
    .hbm_axi_1_wstrb                  (0),
    .hbm_axi_1_wvalid                 (0)   
`endif
);

SERVETop SERVETop(
  .clock (cpu_clk), // @[:freedom.serve.SERVEiFPGAConfig.fir@561594.4]
  .reset (cpu_reset), // @[:freedom.serve.SERVEiFPGAConfig.fir@561595.4]
  /*. io_ps_axi_slave_aw_ready (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_valid ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_id ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_addr ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_len ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_size ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_burst ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_lock ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_cache ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_prot ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_aw_bits_qos ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_w_ready (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_w_valid ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_w_bits_data ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_w_bits_strb ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_w_bits_last ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_b_ready (1'b1), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_b_valid (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_b_bits_id (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_b_bits_resp (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_ready (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_valid ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_id ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_addr ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_len ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_size ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_burst ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_lock ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_cache ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_prot ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_ar_bits_qos ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_r_ready ('d1), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_r_valid (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_r_bits_id (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_r_bits_data (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_r_bits_resp (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_ps_axi_slave_r_bits_last (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]*/
  .io_mac_int (mac_int), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_dma_int (s2mm_introut | mm2s_introut), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_uart_int (uart_int), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .io_usb_int ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  //.io_jtag_TCK ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  //.io_jtag_TMS ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  //.io_jtag_TDI ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  //.io_jtag_TDO_data ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  //.io_jtag_TDO_driven ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  //.io_gpio_btns ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  //.io_gpio_sws ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561597.4]
  .mem_axi_0_aw_ready (mem_axi_0_aw_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_valid (mem_axi_0_aw_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_id (mem_axi_0_aw_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_addr (mem_axi_0_aw_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_len (mem_axi_0_aw_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_size (mem_axi_0_aw_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_burst (mem_axi_0_aw_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_lock (mem_axi_0_aw_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_cache (mem_axi_0_aw_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_prot (mem_axi_0_aw_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_aw_bits_qos (mem_axi_0_aw_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_w_ready (mem_axi_0_w_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_w_valid (mem_axi_0_w_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_w_bits_data (mem_axi_0_w_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_w_bits_strb (mem_axi_0_w_bits_strb), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_w_bits_last (mem_axi_0_w_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_b_ready (mem_axi_0_b_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_b_valid (mem_axi_0_b_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_b_bits_id (mem_axi_0_b_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_b_bits_resp (mem_axi_0_b_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_ready (mem_axi_0_ar_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_valid (mem_axi_0_ar_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_id (mem_axi_0_ar_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_addr (mem_axi_0_ar_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_len (mem_axi_0_ar_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_size (mem_axi_0_ar_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_burst (mem_axi_0_ar_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_lock (mem_axi_0_ar_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_cache (mem_axi_0_ar_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_prot (mem_axi_0_ar_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_ar_bits_qos (mem_axi_0_ar_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_r_ready (mem_axi_0_r_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_r_valid (mem_axi_0_r_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_r_bits_id (mem_axi_0_r_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_r_bits_data (mem_axi_0_r_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_r_bits_resp (mem_axi_0_r_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]
  .mem_axi_0_r_bits_last (mem_axi_0_r_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561598.4]

  .mem_axi_1_aw_ready (mem_axi_1_aw_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_valid (mem_axi_1_aw_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_id (mem_axi_1_aw_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_addr (mem_axi_1_aw_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_len (mem_axi_1_aw_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_size (mem_axi_1_aw_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_burst (mem_axi_1_aw_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_lock (mem_axi_1_aw_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_cache (mem_axi_1_aw_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_prot (mem_axi_1_aw_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_aw_bits_qos (mem_axi_1_aw_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_w_ready (mem_axi_1_w_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_w_valid (mem_axi_1_w_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_w_bits_data (mem_axi_1_w_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_w_bits_strb (mem_axi_1_w_bits_strb), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_w_bits_last (mem_axi_1_w_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_b_ready (mem_axi_1_b_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_b_valid (mem_axi_1_b_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_b_bits_id (mem_axi_1_b_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_b_bits_resp (mem_axi_1_b_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_ready (mem_axi_1_ar_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_valid (mem_axi_1_ar_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_id (mem_axi_1_ar_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_addr (mem_axi_1_ar_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_len (mem_axi_1_ar_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_size (mem_axi_1_ar_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_burst (mem_axi_1_ar_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_lock (mem_axi_1_ar_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_cache (mem_axi_1_ar_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_prot (mem_axi_1_ar_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_ar_bits_qos (mem_axi_1_ar_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_r_ready (mem_axi_1_r_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_r_valid (mem_axi_1_r_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_r_bits_id (mem_axi_1_r_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_r_bits_data (mem_axi_1_r_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_r_bits_resp (mem_axi_1_r_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_1_r_bits_last (mem_axi_1_r_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561599.4]
  .mem_axi_2_aw_ready (mem_axi_2_aw_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_valid (mem_axi_2_aw_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_id (mem_axi_2_aw_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_addr (mem_axi_2_aw_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_len (mem_axi_2_aw_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_size (mem_axi_2_aw_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_burst (mem_axi_2_aw_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_lock (mem_axi_2_aw_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_cache (mem_axi_2_aw_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_prot (mem_axi_2_aw_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_aw_bits_qos (mem_axi_2_aw_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_w_ready (mem_axi_2_w_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_w_valid (mem_axi_2_w_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_w_bits_data (mem_axi_2_w_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_w_bits_strb (mem_axi_2_w_bits_strb), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_w_bits_last (mem_axi_2_w_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_b_ready (mem_axi_2_b_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_b_valid (mem_axi_2_b_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_b_bits_id (mem_axi_2_b_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_b_bits_resp (mem_axi_2_b_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_ready (mem_axi_2_ar_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_valid (mem_axi_2_ar_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_id (mem_axi_2_ar_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_addr (mem_axi_2_ar_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_len (mem_axi_2_ar_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_size (mem_axi_2_ar_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_burst (mem_axi_2_ar_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_lock (mem_axi_2_ar_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_cache (mem_axi_2_ar_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_prot (mem_axi_2_ar_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_ar_bits_qos (mem_axi_2_ar_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_r_ready (mem_axi_2_r_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_r_valid (mem_axi_2_r_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_r_bits_id (mem_axi_2_r_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_r_bits_data (mem_axi_2_r_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_r_bits_resp (mem_axi_2_r_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mem_axi_2_r_bits_last (mem_axi_2_r_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561600.4]
  .mmio_axi_0_aw_ready (mmio_axi_0_aw_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_valid (mmio_axi_0_aw_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_id (mmio_axi_0_aw_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_addr (mmio_axi_0_aw_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_len (mmio_axi_0_aw_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_size (mmio_axi_0_aw_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_burst (mmio_axi_0_aw_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_lock (mmio_axi_0_aw_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_cache (mmio_axi_0_aw_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_prot (mmio_axi_0_aw_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_aw_bits_qos (mmio_axi_0_aw_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_w_ready (mmio_axi_0_w_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_w_valid (mmio_axi_0_w_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_w_bits_data (mmio_axi_0_w_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_w_bits_strb (mmio_axi_0_w_bits_strb), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_w_bits_last (mmio_axi_0_w_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_b_ready (mmio_axi_0_b_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_b_valid (mmio_axi_0_b_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_b_bits_id (mmio_axi_0_b_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_b_bits_resp (mmio_axi_0_b_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_ready (mmio_axi_0_ar_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_valid (mmio_axi_0_ar_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_id (mmio_axi_0_ar_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_addr (mmio_axi_0_ar_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_len (mmio_axi_0_ar_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_size (mmio_axi_0_ar_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_burst (mmio_axi_0_ar_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_lock (mmio_axi_0_ar_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_cache (mmio_axi_0_ar_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_prot (mmio_axi_0_ar_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_ar_bits_qos (mmio_axi_0_ar_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_r_ready (mmio_axi_0_r_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_r_valid (mmio_axi_0_r_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_r_bits_id (mmio_axi_0_r_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_r_bits_data (mmio_axi_0_r_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_r_bits_resp (mmio_axi_0_r_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .mmio_axi_0_r_bits_last (mmio_axi_0_r_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561601.4]
  .front_axi_0_aw_ready (front_axi_0_aw_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_valid (front_axi_0_aw_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_id (front_axi_0_aw_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_addr (front_axi_0_aw_bits_addr[36:0]), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_len (front_axi_0_aw_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_size (front_axi_0_aw_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_burst (front_axi_0_aw_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_lock (front_axi_0_aw_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_cache (front_axi_0_aw_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_prot (front_axi_0_aw_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_aw_bits_qos (front_axi_0_aw_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_w_ready (front_axi_0_w_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_w_valid (front_axi_0_w_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_w_bits_data (front_axi_0_w_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_w_bits_strb (front_axi_0_w_bits_strb), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_w_bits_last (front_axi_0_w_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_b_ready (front_axi_0_b_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_b_valid (front_axi_0_b_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_b_bits_id (front_axi_0_b_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_b_bits_resp (front_axi_0_b_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_ready (front_axi_0_ar_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_valid (front_axi_0_ar_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_id (front_axi_0_ar_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_addr (front_axi_0_ar_bits_addr[36:0]), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_len (front_axi_0_ar_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_size (front_axi_0_ar_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_burst (front_axi_0_ar_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_lock (front_axi_0_ar_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_cache (front_axi_0_ar_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_prot (front_axi_0_ar_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_ar_bits_qos (front_axi_0_ar_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_r_ready (front_axi_0_r_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_r_valid (front_axi_0_r_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_r_bits_id (front_axi_0_r_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_r_bits_data (front_axi_0_r_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_r_bits_resp (front_axi_0_r_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]
  .front_axi_0_r_bits_last (front_axi_0_r_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561602.4]


 .front_axi_1_aw_ready (front_axi_1_aw_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_valid (front_axi_1_aw_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_id (front_axi_1_aw_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_addr (front_axi_1_aw_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_len (front_axi_1_aw_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_size (front_axi_1_aw_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_burst (front_axi_1_aw_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_lock (front_axi_1_aw_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_cache (front_axi_1_aw_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_prot (front_axi_1_aw_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_aw_bits_qos (front_axi_1_aw_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_w_ready (front_axi_1_w_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_w_valid (front_axi_1_w_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_w_bits_data (front_axi_1_w_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_w_bits_strb (front_axi_1_w_bits_strb), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_w_bits_last (front_axi_1_w_bits_last), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_b_ready (front_axi_1_b_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_b_valid (front_axi_1_b_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_b_bits_id (front_axi_1_b_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_b_bits_resp (front_axi_1_b_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_ready (front_axi_1_ar_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_valid (front_axi_1_ar_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_id (front_axi_1_ar_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_addr (front_axi_1_ar_bits_addr), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_len (front_axi_1_ar_bits_len), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_size (front_axi_1_ar_bits_size), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_burst (front_axi_1_ar_bits_burst), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_lock (front_axi_1_ar_bits_lock), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_cache (front_axi_1_ar_bits_cache), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_prot (front_axi_1_ar_bits_prot), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_ar_bits_qos (front_axi_1_ar_bits_qos), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_r_ready (front_axi_1_r_ready), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_r_valid (front_axi_1_r_valid), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_r_bits_id (front_axi_1_r_bits_id), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_r_bits_data (front_axi_1_r_bits_data), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_r_bits_resp (front_axi_1_r_bits_resp), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 .front_axi_1_r_bits_last (front_axi_1_r_bits_last) // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
 
//   .front_axi_1_aw_ready (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_valid (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_id (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_addr (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_len (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_size (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_burst (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_lock (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_cache (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_prot (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_aw_bits_qos (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_w_ready (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_w_valid (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_w_bits_data (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_w_bits_strb (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_w_bits_last (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_b_ready (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_b_valid (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_b_bits_id (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_b_bits_resp (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_ready (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_valid (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_id (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_addr (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_len (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_size (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_burst (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_lock (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_cache (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_prot (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_ar_bits_qos (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_r_ready (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_r_valid (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_r_bits_id (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_r_bits_data (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_r_bits_resp (0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]
//   .front_axi_1_r_bits_last (0) // @[:freedom.serve.SERVEiFPGAConfig.fir@561603.4]

  /*.front_axi_2_aw_ready (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_valid ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_id ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_addr ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_len ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_size ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_burst ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_lock ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_cache ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_prot ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_aw_bits_qos ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_w_ready (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_w_valid ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_w_bits_data ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_w_bits_strb ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_w_bits_last ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_b_ready ('d1), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_b_valid (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_b_bits_id (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_b_bits_resp (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_ready (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_valid ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_id ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_addr ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_len ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_size ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_burst ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_lock ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_cache ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_prot ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_ar_bits_qos ('d0), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_r_ready ('d1), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_r_valid (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_r_bits_id (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_r_bits_data (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_r_bits_resp (), // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]
  .front_axi_2_r_bits_last () // @[:freedom.serve.SERVEiFPGAConfig.fir@561604.4]*/
);

// hbm_0 u_hbm(
// `ifdef _WITH_DDR_
//     .HBM_REF_CLK_0              (RefCLK),
//     .AXI_00_ACLK                (cpu_clk),
//     .AXI_00_ARESET_N            (hbm_resetn),
//     .APB_0_PCLK                 (RefCLK),
//     .APB_0_PRESET_N             (apb_resetn),
// `else
//     .HBM_REF_CLK_0              (RefCLK),
//     //.HBM_REF_CLK_1              (clk_in1_p),
//     .AXI_00_ACLK                (cpu_clk),
//     .AXI_00_ARESET_N            (hbm_resetn),
//     .AXI_01_ACLK                (cpu_clk),
//     .AXI_01_ARESET_N            (hbm_resetn),
//     .APB_0_PCLK                 (RefCLK),
//     //.APB_1_PCLK                 (hbm_apb_clk),
//     .APB_0_PRESET_N             (apb_resetn),
//     //.APB_1_PRESET_N             (apb_resetn),
    
//     .AXI_01_ARADDR              (hbm_axi_1_araddr + 33'h010000000),
//     .AXI_01_ARBURST             (hbm_axi_1_arburst),
//     .AXI_01_ARID                ('d0),
//     .AXI_01_ARLEN               (hbm_axi_1_arlen),
//     .AXI_01_ARSIZE              (hbm_axi_1_arsize),
//     .AXI_01_ARVALID             (hbm_axi_1_arvalid),
//     .AXI_01_AWADDR              (hbm_axi_1_awaddr + 33'h010000000),
//     .AXI_01_AWBURST             (hbm_axi_1_awburst),
//     .AXI_01_AWID                ('d0),
//     .AXI_01_AWLEN               (hbm_axi_1_awlen),
//     .AXI_01_AWSIZE              (hbm_axi_1_awsize),
//     .AXI_01_AWVALID             (hbm_axi_1_awvalid),
//     .AXI_01_RREADY              (hbm_axi_1_rready),
//     .AXI_01_BREADY              (hbm_axi_1_bready),
//     .AXI_01_WDATA               (hbm_axi_1_wdata),
//     .AXI_01_WLAST               (hbm_axi_1_wlast),
//     .AXI_01_WSTRB               (hbm_axi_1_wstrb),
//     .AXI_01_WDATA_PARITY        ('d0),
//     .AXI_01_WVALID              (hbm_axi_1_wvalid),
//     .AXI_01_ARREADY             (hbm_axi_1_arready),
//     .AXI_01_AWREADY             (hbm_axi_1_awready),
//     .AXI_01_RDATA_PARITY        (),
//     .AXI_01_RDATA               (hbm_axi_1_rdata),
//     .AXI_01_RID                 (),
//     .AXI_01_RLAST               (hbm_axi_1_rlast),
//     .AXI_01_RRESP               (hbm_axi_1_rresp),
//     .AXI_01_RVALID              (hbm_axi_1_rvalid),
//     .AXI_01_WREADY              (hbm_axi_1_wready),
//     .AXI_01_BID                 (),
//     .AXI_01_BRESP               (hbm_axi_1_bresp),
//     .AXI_01_BVALID              (hbm_axi_1_bvalid),
//     //.APB_1_PWDATA               ('d0),
//     //.APB_1_PADDR                ('d0),
//     //.APB_1_PENABLE              ('d0),
//     //.APB_1_PSEL                 ('d0),
//     //.APB_1_PWRITE               ('d0),
//     //.APB_1_PRDATA               (),
//     //.APB_1_PREADY               (),
//     //.APB_1_PSLVERR              (),
//     //.apb_complete_1             (apb_complete_1),
//     //.DRAM_1_STAT_CATTRIP        (),
//     //.DRAM_1_STAT_TEMP           (),
// `endif
//     .AXI_00_ARADDR              (hbm_axi_araddr),
//     .AXI_00_ARBURST             (hbm_axi_arburst),
//     .AXI_00_ARID                ('d0),
//     .AXI_00_ARLEN               (hbm_axi_arlen),
//     .AXI_00_ARSIZE              (hbm_axi_arsize),
//     .AXI_00_ARVALID             (hbm_axi_arvalid),
//     .AXI_00_AWADDR              (hbm_axi_awaddr),
//     .AXI_00_AWBURST             (hbm_axi_awburst),
//     .AXI_00_AWID                ('d0),
//     .AXI_00_AWLEN               (hbm_axi_awlen),
//     .AXI_00_AWSIZE              (hbm_axi_awsize),
//     .AXI_00_AWVALID             (hbm_axi_awvalid),
//     .AXI_00_RREADY              (hbm_axi_rready),
//     .AXI_00_BREADY              (hbm_axi_bready),
//     .AXI_00_WDATA               (hbm_axi_wdata),
//     .AXI_00_WLAST               (hbm_axi_wlast),
//     .AXI_00_WSTRB               (hbm_axi_wstrb),
//     .AXI_00_WDATA_PARITY        ('d0),
//     .AXI_00_WVALID              (hbm_axi_wvalid),
//     //.APB_0_PWDATA               (APB_M_0_pwdata),
//     //.APB_0_PADDR                (APB_M_0_paddr[21:0]),
//     //.APB_0_PENABLE              (APB_M_0_penable),
//     //.APB_0_PSEL                 (APB_M_0_psel),
//     //.APB_0_PWRITE               (APB_M_0_pwrite),
//     .APB_0_PWDATA               ('d0),
//     .APB_0_PADDR                ('d0),
//     .APB_0_PENABLE              ('d0),
//     .APB_0_PSEL                 ('d0),
//     .APB_0_PWRITE               ('d0),
    
//     .AXI_00_ARREADY             (hbm_axi_arready),
//     .AXI_00_AWREADY             (hbm_axi_awready),
//     .AXI_00_RDATA_PARITY        (),
//     .AXI_00_RDATA               (hbm_axi_rdata),
//     .AXI_00_RID                 (),
//     .AXI_00_RLAST               (hbm_axi_rlast),
//     .AXI_00_RRESP               (hbm_axi_rresp),
//     .AXI_00_RVALID              (hbm_axi_rvalid),
//     .AXI_00_WREADY              (hbm_axi_wready),
//     .AXI_00_BID                 (),
//     .AXI_00_BRESP               (hbm_axi_bresp),
//     .AXI_00_BVALID              (hbm_axi_bvalid),
//     //.APB_0_PRDATA               (APB_M_0_prdata),
//     //.APB_0_PREADY               (APB_M_0_pready),
//     //.APB_0_PSLVERR              (APB_M_0_pslverr),
//     .apb_complete_0             (apb_complete_0),
//     .DRAM_0_STAT_CATTRIP        (),
//     .DRAM_0_STAT_TEMP           ()
// );

`ifdef _WITH_DDR_

IBUFDS GCIO_CLK_IN(
    .O  (RefCLK  ),
    .I  (clk_in1_p),
    .IB  (clk_in1_n)
);/*
BUFG DDR_CLK_IN(
    .O  (ddr4_RefCLK  ),
    .I  (memory_clk)
);*/
/*BUFG HBM_CLK_IN(
    .O  (RefCLK  ),
    .I  (memory_clk)
);*/
/*memory_clk_src u_m_clk_src(
    .CLK_IN_clk_n               (clk_in1_n),
    .CLK_IN_clk_p               (clk_in1_p),
    .RefCLK                     (RefCLK)
    //.DDR_CLK                    (ddr4_RefCLK)
);*/
clk_src u_clk_src(
    .clk_in1                     (DDR_SYS_CLK_p)
   ,.cpu_clk                    (cpu_clk),
    .spi_clk                    (spi_clk)
    
    );
/*ddr4_0 u_ddr4(
        // AXI CTRL port
    .c0_ddr4_s_axi_ctrl_awvalid         ('d0),
    .c0_ddr4_s_axi_ctrl_awready         (),
    .c0_ddr4_s_axi_ctrl_awaddr          ('d0),
    // Slave Interface Write Data Ports
    .c0_ddr4_s_axi_ctrl_wvalid          ('d0),
    .c0_ddr4_s_axi_ctrl_wready          (),
    .c0_ddr4_s_axi_ctrl_wdata           ('d0),
    // Slave Interface Write Response Ports
    .c0_ddr4_s_axi_ctrl_bvalid          (),
    .c0_ddr4_s_axi_ctrl_bready          (1'b1),
    .c0_ddr4_s_axi_ctrl_bresp           (),
    // Slave Interface Read Address Ports
    .c0_ddr4_s_axi_ctrl_arvalid         ('d0),
    .c0_ddr4_s_axi_ctrl_arready         (),
    .c0_ddr4_s_axi_ctrl_araddr          ('d0),
    // Slave Interface Read Data Ports
    .c0_ddr4_s_axi_ctrl_rvalid          (),
    .c0_ddr4_s_axi_ctrl_rready          (1'b1),
    .c0_ddr4_s_axi_ctrl_rdata           (),
    .c0_ddr4_s_axi_ctrl_rresp           (),

   .c0_ddr4_aresetn             (~c0_ddr4_ui_clk_sync_rst),
   .c0_ddr4_s_axi_awid          (c0_ddr4_s_axi_awid),
   .c0_ddr4_s_axi_awaddr        (c0_ddr4_s_axi_awaddr),
   .c0_ddr4_s_axi_awlen         (c0_ddr4_s_axi_awlen),
   .c0_ddr4_s_axi_awsize        (c0_ddr4_s_axi_awsize),
   .c0_ddr4_s_axi_awburst       (c0_ddr4_s_axi_awburst),
   .c0_ddr4_s_axi_awlock        (c0_ddr4_s_axi_awlock),
   .c0_ddr4_s_axi_awcache       (c0_ddr4_s_axi_awcache),
   .c0_ddr4_s_axi_awprot        (c0_ddr4_s_axi_awprot),
   .c0_ddr4_s_axi_awqos         (c0_ddr4_s_axi_awqos),
   .c0_ddr4_s_axi_awvalid       (c0_ddr4_s_axi_awvalid),
   .c0_ddr4_s_axi_awready       (c0_ddr4_s_axi_awready),
   // Slave Interface Write Data Ports
   .c0_ddr4_s_axi_wdata         (c0_ddr4_s_axi_wdata),
   .c0_ddr4_s_axi_wstrb         (c0_ddr4_s_axi_wstrb),
   .c0_ddr4_s_axi_wlast         (c0_ddr4_s_axi_wlast),
   .c0_ddr4_s_axi_wvalid        (c0_ddr4_s_axi_wvalid),
   .c0_ddr4_s_axi_wready        (c0_ddr4_s_axi_wready),
   // Slave Interface Write Response Ports
   .c0_ddr4_s_axi_bready        (c0_ddr4_s_axi_bready),
   .c0_ddr4_s_axi_bid           (c0_ddr4_s_axi_bid),
   .c0_ddr4_s_axi_bresp         (c0_ddr4_s_axi_bresp),
   .c0_ddr4_s_axi_bvalid        (c0_ddr4_s_axi_bvalid),
   // Slave Interface Read Address Ports
   . c0_ddr4_s_axi_arid         (c0_ddr4_s_axi_arid),
   .c0_ddr4_s_axi_araddr        (c0_ddr4_s_axi_araddr)    ,
   .c0_ddr4_s_axi_arlen         (c0_ddr4_s_axi_arlen),
   .c0_ddr4_s_axi_arsize        (c0_ddr4_s_axi_arsize),
   .c0_ddr4_s_axi_arburst       (c0_ddr4_s_axi_arburst),
   .c0_ddr4_s_axi_arlock        (c0_ddr4_s_axi_arlock),
   .c0_ddr4_s_axi_arcache       (c0_ddr4_s_axi_arcache),
   .c0_ddr4_s_axi_arprot        (c0_ddr4_s_axi_arprot),
   .c0_ddr4_s_axi_arqos         (c0_ddr4_s_axi_arqos),
   .c0_ddr4_s_axi_arvalid       (c0_ddr4_s_axi_arvalid),
   .c0_ddr4_s_axi_arready       (c0_ddr4_s_axi_arready),
   // Slave Interface Read Data Ports
   .c0_ddr4_s_axi_rready        (c0_ddr4_s_axi_rready),
   .c0_ddr4_s_axi_rid           (c0_ddr4_s_axi_rid),
   .c0_ddr4_s_axi_rdata         (c0_ddr4_s_axi_rdata),
   .c0_ddr4_s_axi_rresp         (c0_ddr4_s_axi_rresp),
   .c0_ddr4_s_axi_rlast         (c0_ddr4_s_axi_rlast),
   .c0_ddr4_s_axi_rvalid        (c0_ddr4_s_axi_rvalid),

    .sys_rst                    (reset),
   .c0_sys_clk_i                (RefCLK),
   .c0_ddr4_act_n               (ddr4_sdram_act_n),
   .c0_ddr4_adr                 (ddr4_sdram_adr),
   .c0_ddr4_ba                  (ddr4_sdram_ba),
   .c0_ddr4_bg                  (ddr4_sdram_bg),
   .c0_ddr4_cke                 (ddr4_sdram_cke),
   .c0_ddr4_odt                 (ddr4_sdram_odt),
   .c0_ddr4_cs_n                (ddr4_sdram_cs_n),
   .c0_ddr4_ck_t                (ddr4_sdram_ck_t),
   .c0_ddr4_ck_c                (ddr4_sdram_ck_c),
   .c0_ddr4_reset_n             (ddr4_sdram_reset_n),
   .c0_ddr4_dm_dbi_n            (ddr4_sdram_dm_n),
   .c0_ddr4_dq                  (ddr4_sdram_dq),
   .c0_ddr4_dqs_c               (ddr4_sdram_dqs_c),
   .c0_ddr4_dqs_t               (ddr4_sdram_dqs_t),

   .c0_init_calib_complete      (),
   .c0_ddr4_ui_clk              (c0_ddr4_ui_clk),
   .c0_ddr4_ui_clk_sync_rst     (c0_ddr4_ui_clk_sync_rst),
   .dbg_clk                     ()
);*/
reset_src u_reset_src(
    .apb_resetn                 (apb_resetn),
    .reset               (reset),
    .reset_sync_clk           (RefCLK)
);
 `endif

`ifndef _WITH_DDR_
clk_src u_clk_src(
    .clk_in1                     (DDR_SYS_CLK_p)
   ,.cpu_clk                    (cpu_clk),
    .spi_clk                    (spi_clk),
    // .hbm_interface_clk          (hbm_apb_clk)
    
    //.global_clk1               (hbm_clk1)

);
reset_src u_reset_src(
    .apb_resetn                 (apb_resetn),
    .ext_reset_in               (reset),
    .reset_sync_clk           (RefCLK)
);
`endif

// 2021127 ICT-QL Test for PR (By ICAP)
// Start
reg[34:0] r_addr = 0;
wire[34:0] w_addr;
wire[3:0] w_data_out,w_count_out;
vio_PR vio_PR(
	.clk(cpu_clk),
	.probe_in0({w_count_out,w_data_out})
);

always @(posedge cpu_clk) begin

	//if(~rst_n)
	//	r_addr <= 0;
	//else
		r_addr <= r_addr + 1'b1;

end

assign w_addr = r_addr;

shift shift
(
    .en(0),       
    .clk(cpu_clk),      
    .addr(w_addr[34:23]),     
    .data_out(w_data_out) 
);

count count
(
    .rst(0),      
    .clk(cpu_clk),      
    .count_out(w_count_out)
);

// end

endmodule